FROM node:latest
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
ENV app_port 8080
EXPOSE 8080
CMD ["node","app.js"]
